<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Session;

class AppController extends Controller
{
    public function index()
    {
        return view('home2');
    }
    public function index2()
    {
        return view('home');
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|regex:/^[0-9]{8}$/|unique:users',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
            return redirect()->action('AppController@index')->with('message', false);
        }

        $user = new User;
        $user->name = $request->input('name');
        $user->company = $request->input('company');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');

        if ($user->save()) {
            return response()->json(['success' => true, 'message' => 'Nous vous contacterons dans les plus brefs délais']);
        }
        return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
    }

}
