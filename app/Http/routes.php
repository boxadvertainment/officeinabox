<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses' => 'AppController@index', 'as' => 'home']);
Route::get('/index2', ['uses' => 'AppController@index2', 'as' => 'home2']);
Route::post('/signup', ['uses' => 'AppController@signup', 'as' => 'signup']);

Route::get('admin', ['uses' => 'Admin\AdminController@dashboard', 'as' => 'admin.dashboard']);

