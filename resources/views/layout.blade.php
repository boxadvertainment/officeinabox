<!doctype html>
<!--[if lte IE 8]>     <html class="no-js lte-ie8" lang="{{ App::getLocale() }}"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="favicon.ico" />

    <title>Ooredoo - Office in a box @yield('title', Config::get('app.name'))</title>
    <meta name="description" content="Bénéficiez d’une connexion Internet très haut débit jusqu’à 100 Mbps, d’un mini-standard téléphonique, de forfaits voix partagés et de plein d’autres services professionnels.">
    <meta name="keywords" content="Ooredoo, Tunisie, Téléphone, Internet, Haut débit, Mini-standard, Standard, Professionnels">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Open Graph data -->
    <meta property="og:image" content="http://odyssee.ooredoo.tn/img/fb_share.jpg"/>
    <meta property="og:title" content="Découvrez l'Office in a box"/>
    <meta property="og:url" content="http://www.officeinabox.tn"/>
    <meta property="og:site_name" content="Office in a box"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="Bénéficiez d’une connexion Internet très haut débit, d’un mini-standard téléphonique, de forfaits voix partagés et de plein d’autres services professionnels."/>

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@ooredootn">
    <meta name="twitter:title" content="Découvrez l'Office in a box">
    <meta name="twitter:description" content="Une connexion Internet très haut débit, d’un mini-standard téléphonique, de forfaits voix partagés et d’autres services Pro. @ooredootn">
    <meta name="twitter:image" content="http://odyssee.ooredoo.tn/img/fb_share.jpg">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweet-alert.css') }}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.fullPage.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/main.css') }}?v=1">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    @yield('styles')

    <!--[if lte IE 8]><script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="{{ asset('js/vendor/modernizr-2.6.2.min.js') }}"></script>
</head>
<body class="@yield('class')">

    <div class="animated fadeIn loader">
        <div class="spinner">
            <div class="one"></div>
            <div class="two"></div>
            <div class="three"></div>
        </div>
    </div>

    <audio id="beep-one">
        <source src="audio/beep.mp3">
        <source src="audio/beep.ogg">
    </audio>


    <div class="alert alert-dismissible outdated-browser" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>

    @yield('content')


    @yield('body')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('js/vendor/jquery-1.10.2.min.js') }}"><\/script>')</script>
    <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery.fullPage.js') }}"></script>
    <script src="{{ asset('js/vendor/sweet-alert.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    @yield('scripts')
        

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>

