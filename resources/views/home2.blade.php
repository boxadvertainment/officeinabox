@extends('layout')

@section('class', 'home')

@section('content')


        <div id="header">

            <nav class="navbar">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a href="#accueil" class="main_logo navbar-brand"><img src="img/ooredooBiz.png" alt="Ooredoo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#accueil">Accueil</a></li>
                        <li><a href="#video">La video</a></li>
                        <li><a href="#caracteristiques">Caractéristiques</a></li>
                        <li><a href="#offre">L'offre</a></li>
                        <li class="social-share hidden-xs">

                            <div id="header-sharing" class="social-sharing">
                                <span class="social twitter">
                                    <a target="_blank" class="share-action" href="https://twitter.com/home?status=Office in a box. une connexion Internet haut débit, un mini-standard téléphonique, forfaits voix partagés et autres services Pro. @ooredootn"></a>
                                    <span id="twitter-count" class="count"></span>
                                </span>
                                <span class="social facebook">
                                    <a target="_blank" class="share-action" href="https://www.facebook.com/sharer/sharer.php?u=http://www.officeinabox.tn/?rel=share"></a>
                                    <span id="facebook-count" class="count"></span>
                                </span>
                            </div>

                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>

        </div>

        <div id="call-me" class="hidden-xs" data-toggle="modal" data-target="#call-me-modal">
           <b>La Box </b>vous intéresse ?
        </div>

        <div id="fullpage">

            <section class="section video_container" id="section0">
                <div class="container">
                    <div class="col-md-6 col-sm-6">
                        <h1 class="title">
                            Office in a box<br>
                            <small class="subtitle">Bienvenue dans la cour des grands </small>
                        </h1>
                        <p>Bénéficiez d’une connexion Internet très haut débit jusqu’à 100 Mbps, d’un mini-standard téléphonique, de forfaits voix partagés et de plein d’autres services professionnels.</p>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <img src="img/officeinabox-1.png" width="100%" alt="">
                    </div>
                    <div class="info-scroll">
                        <div class="tooltip_down bounce"> Découvrez-la </div>
                        <a href="#video" class="down-btn btn-4 btn-4a glyphicon glyphicon-chevron-down bounce"></a>
                    </div>
                </div>


            </section>


            <section class="section container video_container" id="section1" style="background-image: url(img/firstBG.jpg) repeat">
                
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h1>Office in a box</h1>
                    <p>Exclusivité en Tunisie ! La 1ère offre fixe complète pour les professionnels & PME à petit prix.</p>
                </div>
                <div class="col-md-6 col-md-offset-3 embed-responsive embed-responsive-16by9 video" style="background: url(img/videocover.jpg) center no-repeat;">
                    <div class="embed-responsive-item video-cover"></div>
                </div>

            </section>


            <section class="section caract" id="section2">
                <div class="container">
                    <div class="col-md-6 col-sm-6">
                        <h1 class="title">Office in a box <br></h1>
                        <ul class="cart-list">
                            <li><i class="glyphicon glyphicon-record"></i> Interfaces Ethernet : 4 ports Ethernet</li>
                            <li><i class="glyphicon glyphicon-record"></i> Connectivité sans fil : Wi-Fi 802.11 b/g/n 2.4Ghz</li>
                            <li><i class="glyphicon glyphicon-record"></i> Interfaces WAN : Port DSL (ADSL2+VDSL2)</li>
                            <li><i class="glyphicon glyphicon-record"></i> Interfaces FTTH (Fibre) : Port 10/100/1000 Base-T</li>
                            <li><i class="glyphicon glyphicon-record"></i> Port USB : 2 ports USB 2.0</li>
                            <li><a href="{{ asset('GuideBoxPro.pdf') }}" target="_blank" class="btn download">Télécharger le PDF</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <img src="img/caract.png" width="100%" alt="">
                    </div>
                        
                    
                </div>
            </section>

            <section class="section" id="section3">
               <div class="container">

                    <div class="col-md-6 col-sm-6 hidden-xs">
                        <img src="img/officeinabox-1.png" width="100%" alt="">
                    </div>

                    <div class="col-md-6 col-sm-6">
                        <h1 class="title">
                            Office in a box<br>
                            <small class="subtitle">À partir de <b>119dt/mois</b></small>
                        </h1>
                        <ul class="cart-list">
                            <li><i class="glyphicon glyphicon-record"></i> Très haut débit jusqu’à 100 Mbps</li>
                            <li><i class="glyphicon glyphicon-record"></i> Standard téléphonique</li>
                            <li><i class="glyphicon glyphicon-record"></i> Forfait Voix Fixe partagé</li>
                            <li><i class="glyphicon glyphicon-record"></i> Jusqu’à 6 lignes fixes et 12 appels simultanés</li>
                            <li><i class="glyphicon glyphicon-record"></i> VPN IPSec et adresses IP Fixes</li>
                            <li><i class="glyphicon glyphicon-record"></i> Services Cloud : Hébergement, stockage et B-mail</li>
                            <li class="visible-xs"><a href="#" class="btn download call" data-toggle="modal" data-target="#call-me-modal"><b>La Box </b>vous intéresse ?</a></li>
                        </ul>
                    </div>

                </div>
            </section>


        </div>

        <footer class="footer black hidden-xs">
            <div class="container-fluid">
                <div id="social" class="hidden-xs">
                    <a href="https://www.facebook.com/ooredootn" id="fb" target="_blank" class="fa fa-facebook"></a>
                    <a href="https://twitter.com/ooredootn" id="tw" target="_blank" class="fa fa-twitter"></a>
                    <a href="http://www.linkedin.com/company/ooredootn" id="li" target="_blank" class="fa fa-linkedin"></a>
                    <a href="http://www.youtube.com/user/ooredootn" id="yt" target="_blank" class="fa fa-youtube"></a>
                </div>
                <div class="copyright">© 2015 <a href="http://www.ooredoo.tn/professionnels/Office-in-a-box" target="_blank">Ooredoo</a></div>
            </div>
        </footer>


    <div class="modal fade" id="call-me-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="modal-title">Cette offre vous intéresse ?</span>
          </div>
          <form id="signup-form" action="{{ action('AppController@signup') }}" method="post">
              <div class="modal-body clearfix">
                  <div class="col-sm-10 col-sm-offset-1">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <p>Renseignez ces champs et nous vous contacterons pour plus d’infos</p>

                      <div class="form-group name">
                          <label class="control-label">Nom et prénom<span class="text-red">*</span> :</label>
                          <div class="inputs">
                              <input type="text" class="form-control" name="name" id="name" maxlength="30" required>
                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                          </div>
                      </div>

                      <div class="form-group name">
                          <label class="control-label">Société :</label>
                          <div class="inputs">
                              <input type="text" class="form-control" name="company" id="company" maxlength="30" >
                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                          </div>
                      </div>

                      <div class="form-group name">
                          <label class="control-label">Email<span class="text-red">*</span> :</label>
                          <div class="inputs">
                              <input type="email" class="form-control" name="email" id="email" required>
                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                          </div>
                      </div>

                      <div class="form-group phone">
                          <label class=" control-label">N° de contact<span class="text-red">*</span> :</label>
                          <div class="inputs">
                              <div class="input-group">
                                  <div class="input-group-addon"><b>+216</b></div>
                                  <input type="tel" class="form-control" name="phone" id="phone" maxlength="8" required>
                              </div>
                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                          </div>
                      </div>

                      <button type="submit" class="btn btn-primary">Valider</button>
                  </div>
              </div>
          </form>
          
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop


